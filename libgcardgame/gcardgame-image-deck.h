/*
 * gcardgame-image-deck.h: card game writing library.
 *
 * Copyright (C) 2002 Mark McLoughlin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *     Mark McLoughlin <mark@skynet.ie>
 */

#ifndef __GCARDGAME_IMAGE_DECK_H__
#define __GCARDGAME_IMAGE_DECK_H__

#include <glib/gmacros.h>
#include <glib-object.h>
#include <gdk/gdk.h>

#include "gcardgame-deck.h"
#include "gcardgame-card-image.h"

G_BEGIN_DECLS

/*
 * The structure describing the layout grid of a card.
 */
typedef struct {
	gdouble width, height;
	gdouble corner;

	/*
	 * x and y co-ordinates of the layout grid
	 * for the medium sized suits in the middle
	 * of the card
	 */
	gdouble x0, x1, x2;
	gdouble y0, y1, y2, y3, y4, y5, y6, y7, y8;

	/*
	 * The position of the rank and small suit images
	 * at the side of the card
	 */
	gdouble rx, ry;
	gdouble sx, sy;
} GCardGameImageLayout;

#define GCARDGAME_TYPE_IMAGE_DECK         (gcardgame_image_deck_get_type ())
#define GCARDGAME_IMAGE_DECK(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), GCARDGAME_TYPE_IMAGE_DECK, GCardGameImageDeck))
#define GCARDGAME_IMAGE_DECK_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), GCARDGAME_TYPE_IMAGE_DECK, GCardGameImageDeckClass))
#define GCARDGAME_IS_IMAGE_DECK(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), GCARDGAME_TYPE_IMAGE_DECK))
#define GCARDGAME_IS_IMAGE_DECK_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), GCARDGAME_TYPE_IMAGE_DECK))
#define GCARDGAME_IMAGE_DECK_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GCARDGAME_TYPE_IMAGE_DECK, GCardGameImageDeckClass))

typedef struct _GCardGameImageDeck          GCardGameImageDeck;
typedef struct _GCardGameImageDeckClass     GCardGameImageDeckClass;
typedef struct _GCardGameImageDeckPrivate   GCardGameImageDeckPrivate;

struct _GCardGameImageDeck {
	GObject                     parent_instance;

	GCardGameImageDeckPrivate  *priv;
};

struct _GCardGameImageDeckClass {
	GObjectClass                parent_class;

	void (* pixbufs_changed) (GCardGameImageDeck);
};

GType                gcardgame_image_deck_get_type     (void) G_GNUC_CONST;

GCardGameImageDeck  *gcardgame_image_deck_new          (void);

void                 gcardgame_image_deck_deal_stack   (GCardGameImageDeck *deck,
							GCardGameStack     *stack,
							GdkDrawable        *drawable,
							gint                x,
							gint                y,
							gint                width,
							gint                height);

void                 gcardgame_image_deck_deal_card    (GCardGameImageDeck *deck,
							GCardGameCard	   card,
							GdkDrawable        *drawable,
							gint                x,
							gint                y,
							gint                width,
							gint                height);

GCardGameCardImage  *gcardgame_image_deck_get_card     (GCardGameImageDeck  *deck,
							GCardGameCard        card);

void                 gcardgame_image_deck_select_card  (GCardGameImageDeck  *deck,
							GCardGameCard        card);

GCardGameImageLayout *gcardgame_image_deck_get_layout  (GCardGameImageDeck  *deck);
							

/*
 * Pixbuf accessors
 */
GdkPixbuf           *gcardgame_image_deck_back_pixbuf  (GCardGameImageDeck  *deck,
							gboolean             rotate);

GdkPixbuf           *gcardgame_image_deck_joker_pixbuf (GCardGameImageDeck  *deck,
							gboolean             rotate);

GdkPixbuf           *gcardgame_image_deck_honor_pixbuf (GCardGameImageDeck  *deck,
							GCardGameCardSuit    suit,
							GCardGameCardValue   value,
							gboolean             rotate);

GdkPixbuf           *gcardgame_image_deck_rank_pixbuf  (GCardGameImageDeck  *deck,
							GCardGameCardSuit    suit,
							GCardGameCardValue   value,
							gboolean             rotate);

GdkPixbuf           *gcardgame_image_deck_small_suit_pixbuf  (GCardGameImageDeck  *deck,
							      GCardGameCardSuit    suit,
							      gboolean             rotate);

GdkPixbuf           *gcardgame_image_deck_medium_suit_pixbuf (GCardGameImageDeck  *deck,
							      GCardGameCardSuit    suit,
							      gboolean             rotate);

GdkPixbuf           *gcardgame_image_deck_large_suit_pixbuf  (GCardGameImageDeck  *deck,
							      GCardGameCardSuit    suit,
							      gboolean             rotate);

G_END_DECLS

#endif /* __GCARDGAME_IMAGE_DECK_H__ */
