/*
 * gcardgame-image-deck.c: card game writing library.
 *
 * Copyright (C) 2002 Mark McLoughlin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *     Mark McLoughlin <mark@skynet.ie>
 */

/*
 * This is broadly a port to the canvas of the gdk-card-image library by:
 *
 * 	Felix Bellaby <felix@pooh.u-net.com> and
 *	Ryu Changwoo <cwryu@eve.kaist.ac.kr>
 *
 * see gnome-games/freecell/gdk-card-image
 */

#include "config.h"
#include <string.h>

#include "gcardgame-image-deck.h"
#include "gcardgame-marshal.h"

#undef GCARDGAME_IMAGE_DECK_DEBUG

/*
 * The number of rows and columns in each
 * type of file. Yes .. this is lame :/
 */
#define BACKS_ROWS            1
#define BACKS_COLS            1
#define HONORS_ROWS           3
#define HONORS_COLS           4
#define JOKERS_ROWS           1
#define JOKERS_COLS           1
#define RANKS_ROWS            2
#define RANKS_COLS            14
#define SMALL_SUITS_ROWS      1
#define SMALL_SUITS_COLS      4
#define MEDIUM_SUITS_ROWS     1
#define MEDIUM_SUITS_COLS     4
#define LARGE_SUITS_ROWS      1
#define LARGE_SUITS_COLS      4

/*
 * Where each type of file is installed. Yes ... I know :-)
 */
#define BACKS_DIR             "backs"
#define HONORS_DIR            "honors"
#define JOKERS_DIR            "jokers"
#define RANKS_DIR             "ranks"
#define SMALL_SUITS_DIR       "small-suits"
#define MEDIUM_SUITS_DIR      "medium-suits"
#define LARGE_SUITS_DIR       "large-suits"

struct _GCardGameImageDeckPrivate {
	GdkPixbuf            *back_pixbufs [BACKS_ROWS * BACKS_COLS * 2];
	GdkPixbuf            *honors_pixbufs [HONORS_ROWS * HONORS_COLS * 2];
	GdkPixbuf            *joker_pixbufs [JOKERS_ROWS * JOKERS_COLS * 2];
	GdkPixbuf            *ranks_pixbufs [RANKS_ROWS * RANKS_COLS * 2];
	GdkPixbuf            *suits_small_pixbufs [SMALL_SUITS_ROWS * SMALL_SUITS_COLS * 2];
	GdkPixbuf            *suits_medium_pixbufs [MEDIUM_SUITS_ROWS * MEDIUM_SUITS_COLS * 2];
	GdkPixbuf            *suits_large_pixbufs [LARGE_SUITS_ROWS * LARGE_SUITS_COLS * 2];

	gchar                *back_file;
	gchar                *honors_file;
	gchar                *joker_file;
	gchar                *ranks_file;
	gchar                *suits_small_file;
	gchar                *suits_medium_file;
	gchar                *suits_large_file;

	gboolean              layout_needs_calc;
	GCardGameImageLayout  layout;
};

static GObjectClass *parent_class;

enum {
	PIXBUFS_CHANGED,
        LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_BACK_IMAGE,
	PROP_HONORS_IMAGE,
	PROP_JOKER_IMAGE,
	PROP_RANKS_IMAGE,
	PROP_SUITS_SMALL_IMAGE,
	PROP_SUITS_MEDIUM_IMAGE,
	PROP_SUITS_LARGE_IMAGE
};

static guint gcardgame_image_deck_signals [LAST_SIGNAL];

static void     calculate_card_layout          (GCardGameImageDeck  *deck);
static void     load_pixbuf_and_split          (GCardGameImageDeck  *deck,
						const char          *pixbuf_file,
						const char          *pixbuf_dir,
						GdkPixbuf          **pixbuf_array,
						int                  num_rows,
						int                  num_columns);

static void
load_pixbuf_from_property (GCardGameImageDeck  *deck,
			   const char          *prop_name,
			   const char          *pixbuf_dir,
			   GdkPixbuf          **pixbuf_array,
			   int                  num_rows,
			   int                  num_columns)
{
	GValue      value = { 0, };
	const char *val;

	g_value_init (&value, G_TYPE_STRING);
	g_object_get_property (G_OBJECT (deck), prop_name, &value);
	val = g_value_get_string (&value);

	g_return_if_fail (val);

	load_pixbuf_and_split (deck, val, pixbuf_dir, pixbuf_array, num_rows, num_columns);

	g_value_unset (&value);
}

static void
ensure_pixbufs_and_layout (GCardGameImageDeck *deck)
{
	GCardGameImageDeckPrivate *priv = deck->priv;

	if (!priv->back_pixbufs [0])
		load_pixbuf_from_property (deck, "back-image", BACKS_DIR,
					   deck->priv->back_pixbufs,
					   BACKS_ROWS, BACKS_COLS);
		
	if (!priv->honors_pixbufs [0])
		load_pixbuf_from_property (deck, "honors-image", HONORS_DIR,
					   deck->priv->honors_pixbufs,
					   HONORS_ROWS, HONORS_COLS);

	if (!priv->joker_pixbufs [0])
		load_pixbuf_from_property (deck, "joker-image", JOKERS_DIR,
					   deck->priv->joker_pixbufs,
					   JOKERS_ROWS, JOKERS_COLS);

	if (!priv->ranks_pixbufs [0])
		load_pixbuf_from_property (deck, "ranks-image", RANKS_DIR,
					   deck->priv->ranks_pixbufs,
					   RANKS_ROWS, RANKS_COLS);

	if (!priv->suits_small_pixbufs [0])
		load_pixbuf_from_property (deck, "suits-small-image", SMALL_SUITS_DIR,
					   deck->priv->suits_small_pixbufs,
					   SMALL_SUITS_ROWS, SMALL_SUITS_COLS);

	if (!priv->suits_medium_pixbufs [0])
		load_pixbuf_from_property (deck, "suits-medium-image", MEDIUM_SUITS_DIR,
					   deck->priv->suits_medium_pixbufs,
					   MEDIUM_SUITS_ROWS, MEDIUM_SUITS_COLS);

	if (!priv->suits_large_pixbufs [0])
		load_pixbuf_from_property (deck, "suits-large-image", LARGE_SUITS_DIR,
					   deck->priv->suits_large_pixbufs,
					   LARGE_SUITS_ROWS, LARGE_SUITS_COLS);

	if (priv->layout_needs_calc)
		calculate_card_layout (deck);
}

GCardGameImageLayout *
gcardgame_image_deck_get_layout (GCardGameImageDeck *deck)
{
	g_return_val_if_fail (GCARDGAME_IS_IMAGE_DECK (deck), NULL);

	ensure_pixbufs_and_layout (deck);

	return &deck->priv->layout;
}

GdkPixbuf *
gcardgame_image_deck_back_pixbuf (GCardGameImageDeck *deck,
				  gboolean            rotate)
{
	int index = 0;

	g_return_val_if_fail (GCARDGAME_IS_IMAGE_DECK (deck), NULL);

	ensure_pixbufs_and_layout (deck);

	if (rotate)
		index++;

	return g_object_ref (G_OBJECT (deck->priv->back_pixbufs [index]));
}

GdkPixbuf *
gcardgame_image_deck_joker_pixbuf (GCardGameImageDeck *deck,
				   gboolean            rotate)
{
	int index = 0;

	g_return_val_if_fail (GCARDGAME_IS_IMAGE_DECK (deck), NULL);

	ensure_pixbufs_and_layout (deck);

	if (rotate)
		index++;

	return g_object_ref (G_OBJECT (deck->priv->joker_pixbufs [index]));
}

GdkPixbuf *
gcardgame_image_deck_honor_pixbuf (GCardGameImageDeck *deck,
				   GCardGameCardSuit   suit,
				   GCardGameCardValue  value,
				   gboolean            rotate)
{
	int index = 0;

	g_return_val_if_fail (GCARDGAME_IS_IMAGE_DECK (deck), NULL);
	g_return_val_if_fail (suit >= GCARDGAME_SUIT_CLUBS && suit <= GCARDGAME_SUIT_SPADES, NULL);
	g_return_val_if_fail (value >= GCARDGAME_VALUE_JACK && value <= GCARDGAME_VALUE_KING, NULL);

	ensure_pixbufs_and_layout (deck);

	index = (suit - 1) * 2 + (value - 10 - 1) * 8;

	if (rotate)
		index++;

	return g_object_ref (G_OBJECT (deck->priv->honors_pixbufs [index]));
}

GdkPixbuf *
gcardgame_image_deck_rank_pixbuf (GCardGameImageDeck *deck,
				  GCardGameCardSuit   suit,
				  GCardGameCardValue  value,
				  gboolean            rotate)
{
	int index = 0;

	g_return_val_if_fail (GCARDGAME_IS_IMAGE_DECK (deck), NULL);
	g_return_val_if_fail (suit >= GCARDGAME_SUIT_CLUBS && suit <= GCARDGAME_SUIT_BLACK, NULL);
	g_return_val_if_fail (value >= GCARDGAME_VALUE_ACE && value <= GCARDGAME_VALUE_JOKER, NULL);

	ensure_pixbufs_and_layout (deck);

	/*
	 * Joker is at index 0.
	 */
	if (value != GCARDGAME_VALUE_JOKER)
		index = value * 2;

	switch (suit) {
	case GCARDGAME_SUIT_DIAMONDS:
	case GCARDGAME_SUIT_HEARTS:
	case GCARDGAME_SUIT_RED:
		index += 14 * 2;
		break;
	default:
		break;
	}

	if (rotate)
		index++;

	return g_object_ref (G_OBJECT (deck->priv->ranks_pixbufs [index]));
}
                                                              
GdkPixbuf *
gcardgame_image_deck_small_suit_pixbuf (GCardGameImageDeck *deck,
					GCardGameCardSuit   suit,
					gboolean            rotate)
{
	int index = 0;

	g_return_val_if_fail (GCARDGAME_IS_IMAGE_DECK (deck), NULL);
	g_return_val_if_fail (suit >= GCARDGAME_SUIT_CLUBS && suit <= GCARDGAME_SUIT_SPADES, NULL);

	ensure_pixbufs_and_layout (deck);

	index = (suit - 1) * 2;

	if (rotate)
		index++;

	return g_object_ref (G_OBJECT (deck->priv->suits_small_pixbufs [index]));
}

GdkPixbuf *
gcardgame_image_deck_medium_suit_pixbuf (GCardGameImageDeck *deck,
					 GCardGameCardSuit   suit,
					 gboolean            rotate)
{
	int index = 0;

	g_return_val_if_fail (GCARDGAME_IS_IMAGE_DECK (deck), NULL);
	g_return_val_if_fail (suit >= GCARDGAME_SUIT_CLUBS && suit <= GCARDGAME_SUIT_SPADES, NULL);

	ensure_pixbufs_and_layout (deck);

	index = (suit - 1) * 2;

	if (rotate)
		index++;

	return g_object_ref (G_OBJECT (deck->priv->suits_medium_pixbufs [index]));
}

GdkPixbuf *
gcardgame_image_deck_large_suit_pixbuf (GCardGameImageDeck *deck,
				        GCardGameCardSuit   suit,
					gboolean            rotate)
{
	int index = 0;

	g_return_val_if_fail (GCARDGAME_IS_IMAGE_DECK (deck), NULL);
	g_return_val_if_fail (suit >= GCARDGAME_SUIT_CLUBS && suit <= GCARDGAME_SUIT_SPADES, NULL);

	ensure_pixbufs_and_layout (deck);

	index = (suit - 1) * 2;

	if (rotate)
		index++;

	return g_object_ref (G_OBJECT (deck->priv->suits_large_pixbufs [index]));
}

/*
 * FIXME:
 * Because the card layout is based only
 * on the honors, small suits, medium suits
 * and ranks geometry, we should scale backs
 * to width * height and make sure large suits
 * and jokers are scaled to < width - xdelta
 * * < height - xdelta.
 */
static void
calculate_card_layout (GCardGameImageDeck *deck)
{
	GCardGameImageLayout *layout;
	gdouble               xdelta;
	gdouble               ydelta;
	int                   honor_width, honor_height;
	int                   medium_suit_width, medium_suit_height;
	int                   small_suit_width;
	int                   rank_width, rank_height;

	g_return_if_fail (deck->priv->honors_pixbufs [0]);
	g_return_if_fail (deck->priv->suits_medium_pixbufs [0]);
	g_return_if_fail (deck->priv->suits_small_pixbufs [0]);
	g_return_if_fail (deck->priv->ranks_pixbufs [0]);

	layout = &deck->priv->layout;

	honor_width        = gdk_pixbuf_get_width (deck->priv->honors_pixbufs [0]);
	honor_height       = gdk_pixbuf_get_height (deck->priv->honors_pixbufs [0]);
	medium_suit_width  = gdk_pixbuf_get_width (deck->priv->suits_medium_pixbufs [0]);
	medium_suit_height = gdk_pixbuf_get_height (deck->priv->suits_medium_pixbufs [0]);
	small_suit_width   = gdk_pixbuf_get_width (deck->priv->suits_small_pixbufs [0]);
	rank_width         = gdk_pixbuf_get_width (deck->priv->ranks_pixbufs [0]);
	rank_height        = gdk_pixbuf_get_height (deck->priv->ranks_pixbufs [0]);

	xdelta = (honor_width - 4) / 3;

	layout->width  = honor_width + 2 * xdelta;
	layout->height = 2 * honor_height - 1 + 2 * xdelta;
	layout->corner = ((2 * xdelta) / 5) * 2;

	ydelta = layout->height / 10;  

	layout->x1 = (layout->width - medium_suit_width) / 2;
	layout->y4 = (layout->height - medium_suit_height) / 2;

	layout->x0 = layout->x1 - xdelta; 
	layout->x2 = layout->x1 + xdelta; 

	layout->y0 = layout->y4 - 3 * ydelta;
	layout->y1 = layout->y4 - 2 * ydelta;
	layout->y3 = layout->y4 - 1 * ydelta;
	layout->y5 = layout->y4 + 1 * ydelta;
	layout->y7 = layout->y4 + 2 * ydelta;
	layout->y8 = layout->y4 + 3 * ydelta;

	layout->y2 = layout->y4 - (3 * ydelta) / 2;
	layout->y6 = layout->y4 + (3 * ydelta) / 2;

	layout->sx = layout->x0 + (medium_suit_width - small_suit_width) / 2 - xdelta; 
	layout->sy = layout->y0 + (medium_suit_width - small_suit_width) / 2;
	layout->rx = layout->sx + (small_suit_width - rank_width) / 2;
	layout->ry = layout->sy - rank_height - 1;

	if (layout->ry < 6) {
		layout->ry = 6;
		layout->sy = layout->ry + rank_height + 1;
	}

	deck->priv->layout_needs_calc = FALSE;

#ifdef GCARDGAME_IMAGE_DECK_DEBUG

	g_print ("honor_width = %d\n", honor_width);
	g_print ("honor_height = %d\n", honor_height);
	g_print ("medium_suit_width = %d\n", medium_suit_width);
	g_print ("medium_suit_height = %d\n", medium_suit_height);
	g_print ("small_suit_width = %d\n", small_suit_width);
	g_print ("small_suit_height = %d\n", gdk_pixbuf_get_height (deck->priv->suits_small_pixbufs [0]));
	g_print ("rank_width = %d\n", rank_width);
	g_print ("rank_height = %d\n", rank_height);
	g_print ("xdelta = %f\n", xdelta);
	g_print ("ydelta = %f\n", ydelta);
	g_print ("width = %f\n", layout->width);
	g_print ("height = %f\n", layout->height);
	g_print ("corner = %f\n", layout->corner);
	g_print ("x0 = %f\n", layout->x0);
	g_print ("x1 = %f\n", layout->x1);
	g_print ("x2 = %f\n", layout->x2);
	g_print ("y0 = %f\n", layout->y0);
	g_print ("y1 = %f\n", layout->y1);
	g_print ("y2 = %f\n", layout->y2);
	g_print ("y3 = %f\n", layout->y3);
	g_print ("y4 = %f\n", layout->y4);
	g_print ("y5 = %f\n", layout->y5);
	g_print ("y6 = %f\n", layout->y6);
	g_print ("y7 = %f\n", layout->y7);
	g_print ("y8 = %f\n", layout->y8);
	g_print ("sx = %f\n", layout->sx);
	g_print ("sy = %f\n", layout->sy);
	g_print ("rx = %f\n", layout->rx);
	g_print ("ry = %f\n", layout->ry);

#endif /*GCARDGAME_IMAGE_DECK_DEBUG */
}

static void
free_pixbuf_array (GdkPixbuf **pixbuf_array,
		   int         num_rows,
		   int         num_columns)
{
	int i, j;

	if (!pixbuf_array [0])
		return;

	for (i = 0; i < num_rows; i++)
		for (j = 0; j < num_columns; j++) {
			int index = i * num_columns * 2 + j * 2;

			g_object_unref (G_OBJECT (pixbuf_array [index]));
			g_object_unref (G_OBJECT (pixbuf_array [index + 1]));

			pixbuf_array [index] = pixbuf_array [index] = NULL;
		}
}

static void
free_pixbuf_buffer (guchar *pixels, gpointer data)
{
	g_free (pixels);
}

static GdkPixbuf *
rotate_pixbuf_180 (GdkPixbuf *pixbuf)
{
	GdkPixbuf *result;
	guchar    *pixels, *new_pixels;
	int        width, height, n_channels, rowstride;
	int        bits_per_sample, bytes_per_pixel;
	int        i, j;

	height    = gdk_pixbuf_get_height (pixbuf);
	rowstride = gdk_pixbuf_get_rowstride (pixbuf);

	new_pixels = g_try_malloc (height * rowstride);
	if (!new_pixels)
		return NULL;

	pixels          = gdk_pixbuf_get_pixels (pixbuf);
	width           = gdk_pixbuf_get_width (pixbuf);
	n_channels      = gdk_pixbuf_get_n_channels (pixbuf);
	bits_per_sample = gdk_pixbuf_get_bits_per_sample (pixbuf);

	bytes_per_pixel = (n_channels * bits_per_sample) / 8;

	for (i = 0; i < height; i++)
		for (j = 0; j < width; j++)
			memcpy (new_pixels + i * rowstride + j * bytes_per_pixel,
				pixels + (height - i - 1) * rowstride + (width - j - 1) * bytes_per_pixel,
				bytes_per_pixel);

	result = gdk_pixbuf_new_from_data (new_pixels,
					   gdk_pixbuf_get_colorspace (pixbuf),
					   gdk_pixbuf_get_has_alpha (pixbuf),
					   bits_per_sample, width, height, rowstride,
					   free_pixbuf_buffer, NULL);

	return result;
}

/*
 * This is here instead of creating a dependancy on libgnome
 *
 * FIXME: only_if_exists == TRUE code path not tested.
 */
static char *
locate_pixmap_file (const char *file,
		    gboolean    only_if_exists)
{
	char        *retval;
	char       **gnome_path;
	char       **tmpv;
	const char  *tmp;

	retval = g_strdup_printf ("%s/%s", GCARDGAME_PIXMAP_DIR, file);

	if (!only_if_exists || g_file_test (retval, G_FILE_TEST_EXISTS))
		return retval;

	g_free (retval);

	tmp = g_getenv ("GNOME2_PATH");

	gnome_path = NULL;
	if (tmp)
		gnome_path = g_strsplit (tmp, ":", -1);

	for (tmpv = gnome_path; tmpv && *tmpv; tmpv++) {
		retval = g_strdup_printf ("%s/share/pixmaps", *tmpv);

		if (g_file_test (retval, G_FILE_TEST_EXISTS)) {
			g_strfreev (gnome_path);
			return retval;
		}

		g_free (retval);
	}

	g_strfreev (gnome_path);

	return NULL;
}

static void
load_pixbuf_and_split (GCardGameImageDeck  *deck,
		       const char          *pixbuf_file,
		       const char          *pixbuf_dir,
		       GdkPixbuf          **pixbuf_array,
		       int                  num_rows,
		       int                  num_columns)
{
	GError    *error = NULL;
	GdkPixbuf *pixbuf;
	char      *tmp_file1;
	char      *tmp_file2;
	int        i, j;
	int        width;
	int        height;

	g_return_if_fail (GCARDGAME_IS_IMAGE_DECK (deck));
	g_return_if_fail (pixbuf_dir && pixbuf_file);
	g_return_if_fail (pixbuf_array);

	tmp_file1 = g_strdup_printf ("%s/%s", pixbuf_dir, pixbuf_file);
	tmp_file2 = locate_pixmap_file (tmp_file1, TRUE);
	if (!tmp_file2) {
		g_warning (G_STRLOC ": cannot find pixmap %s", tmp_file1);
		g_free (tmp_file1);

		return;
	}

	pixbuf = gdk_pixbuf_new_from_file (tmp_file2, &error);

	g_free (tmp_file1);
	g_free (tmp_file2);

	if (error) {
		g_warning (G_STRLOC ": cannot load %s : %s", pixbuf_file, error->message);
		g_error_free (error);
		error = NULL;

		for (i = 0; i < 2 * (num_rows * num_columns); i++)
			pixbuf_array [i] = NULL;

		return;
	}

	if (pixbuf_array [0])
		free_pixbuf_array (pixbuf_array, num_rows, num_columns);

	deck->priv->layout_needs_calc = TRUE;

	if (num_rows == 1 && num_columns == 1) {
		/* Note the extra ref here */
		pixbuf_array [0] = pixbuf_array [1] = g_object_ref (G_OBJECT (pixbuf));
		return;
	}

	width  = gdk_pixbuf_get_width  (pixbuf) / num_columns;
	height = gdk_pixbuf_get_height (pixbuf) / num_rows;

	for (i = 0; i < num_rows; i++)
		for (j = 0; j < num_columns; j++) {
			int index = i * num_columns * 2 + j * 2;

			pixbuf_array [index] =
				gdk_pixbuf_new_subpixbuf (pixbuf, width * j, height * i, width, height);

			pixbuf_array [index + 1] = rotate_pixbuf_180 (pixbuf_array [index]);
		}

	g_object_unref (G_OBJECT (pixbuf));
}

static void
gcardgame_image_deck_set_property (GObject      *object,
				   guint         prop_id,
				   const GValue *value,
				   GParamSpec   *pspec)
{
	GCardGameImageDeck *deck = GCARDGAME_IMAGE_DECK (object);

	switch (prop_id) {
	case PROP_BACK_IMAGE:
		if (deck->priv->back_file)
			g_free (deck->priv->back_file);

		deck->priv->back_file = g_strdup (g_value_get_string (value));

		if (deck->priv->back_pixbufs [0])
			load_pixbuf_and_split (deck, deck->priv->back_file,
					       BACKS_DIR,
					       deck->priv->back_pixbufs,
					       BACKS_ROWS, BACKS_COLS);
		break;
	case PROP_HONORS_IMAGE:
		if (deck->priv->honors_file)
			g_free (deck->priv->honors_file);

		deck->priv->honors_file = g_strdup (g_value_get_string (value));

		if (deck->priv->honors_pixbufs [0])
			load_pixbuf_and_split (deck, deck->priv->honors_file,
					       HONORS_DIR,
					       deck->priv->honors_pixbufs,
					       HONORS_ROWS, HONORS_COLS);
		break;
	case PROP_JOKER_IMAGE:
		if (deck->priv->joker_file)
			g_free (deck->priv->joker_file);

		deck->priv->joker_file = g_strdup (g_value_get_string (value));

		if (deck->priv->joker_pixbufs [0])
			load_pixbuf_and_split (deck, deck->priv->joker_file,
					       JOKERS_DIR,
					       deck->priv->joker_pixbufs,
					       JOKERS_ROWS, JOKERS_COLS);
		break;
	case PROP_RANKS_IMAGE:
		if (deck->priv->ranks_file)
			g_free (deck->priv->ranks_file);

		deck->priv->ranks_file = g_strdup (g_value_get_string (value));

		if (deck->priv->ranks_pixbufs [0])
			load_pixbuf_and_split (deck, deck->priv->ranks_file,
					       RANKS_DIR,
					       deck->priv->ranks_pixbufs,
					       RANKS_ROWS, RANKS_COLS);
		break;
	case PROP_SUITS_SMALL_IMAGE:
		if (deck->priv->suits_small_file)
			g_free (deck->priv->suits_small_file);
		deck->priv->suits_small_file = g_strdup (g_value_get_string (value));

		if (deck->priv->suits_small_pixbufs [0])
			load_pixbuf_and_split (deck, deck->priv->suits_small_file,
					       SMALL_SUITS_DIR,
					       deck->priv->suits_small_pixbufs,
					       SMALL_SUITS_ROWS, SMALL_SUITS_COLS);
		break;
	case PROP_SUITS_MEDIUM_IMAGE:
		if (deck->priv->suits_medium_file)
			g_free (deck->priv->suits_medium_file);
		deck->priv->suits_medium_file = g_strdup (g_value_get_string (value));

		if (deck->priv->suits_medium_pixbufs [0])
			load_pixbuf_and_split (deck, deck->priv->suits_medium_file,
					       MEDIUM_SUITS_DIR,
					       deck->priv->suits_medium_pixbufs,
					       MEDIUM_SUITS_ROWS, MEDIUM_SUITS_COLS);
		break;
	case PROP_SUITS_LARGE_IMAGE:
		if (deck->priv->suits_large_file)
			g_free (deck->priv->suits_large_file);

		deck->priv->suits_large_file = g_strdup (g_value_get_string (value));

		if (deck->priv->suits_large_pixbufs [0])
			load_pixbuf_and_split (deck, deck->priv->suits_large_file,
					       LARGE_SUITS_DIR,
					       deck->priv->suits_large_pixbufs,
					       LARGE_SUITS_ROWS, LARGE_SUITS_COLS);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
gcardgame_image_deck_get_property (GObject    *object,
				   guint       prop_id,
				   GValue     *value,
				   GParamSpec *pspec)
{
	GCardGameImageDeck *deck = GCARDGAME_IMAGE_DECK (object);

	switch (prop_id) {
	case PROP_BACK_IMAGE:
		g_value_set_string (value, deck->priv->back_file);
		break;
	case PROP_HONORS_IMAGE:
		g_value_set_string (value, deck->priv->honors_file);
		break;
	case PROP_JOKER_IMAGE:
		g_value_set_string (value, deck->priv->joker_file);
		break;
	case PROP_RANKS_IMAGE:
		g_value_set_string (value, deck->priv->ranks_file);
		break;
	case PROP_SUITS_SMALL_IMAGE:
		g_value_set_string (value, deck->priv->suits_small_file);
		break;
	case PROP_SUITS_MEDIUM_IMAGE:
		g_value_set_string (value, deck->priv->suits_medium_file);
		break;
	case PROP_SUITS_LARGE_IMAGE:
		g_value_set_string (value, deck->priv->suits_large_file);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
gcardgame_image_deck_finalize (GObject *object)
{
	GCardGameImageDeck *deck = GCARDGAME_IMAGE_DECK (object);

	free_pixbuf_array (deck->priv->back_pixbufs, BACKS_ROWS, BACKS_COLS);
	free_pixbuf_array (deck->priv->honors_pixbufs, HONORS_ROWS, HONORS_COLS);
	free_pixbuf_array (deck->priv->joker_pixbufs, JOKERS_ROWS, JOKERS_COLS);
	free_pixbuf_array (deck->priv->ranks_pixbufs, RANKS_ROWS, RANKS_COLS);
	free_pixbuf_array (deck->priv->suits_small_pixbufs, SMALL_SUITS_ROWS, SMALL_SUITS_COLS);
	free_pixbuf_array (deck->priv->suits_medium_pixbufs, MEDIUM_SUITS_ROWS, MEDIUM_SUITS_COLS);
	free_pixbuf_array (deck->priv->suits_large_pixbufs, LARGE_SUITS_ROWS, LARGE_SUITS_COLS);

	g_free (deck->priv->back_file);
	g_free (deck->priv->honors_file);
	g_free (deck->priv->joker_file);
	g_free (deck->priv->ranks_file);
	g_free (deck->priv->suits_small_file);
	g_free (deck->priv->suits_medium_file);
	g_free (deck->priv->suits_large_file);

	g_free (deck->priv);
	deck->priv = NULL;

	parent_class->finalize (object);
}

static void
gcardgame_image_deck_class_init (GCardGameImageDeckClass *klass,
				 gpointer                  dummy)
{
	GObjectClass  *gobject_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	gobject_class->finalize     = gcardgame_image_deck_finalize;
	gobject_class->set_property = gcardgame_image_deck_set_property;
	gobject_class->get_property = gcardgame_image_deck_get_property;

	g_object_class_install_property (
			gobject_class, PROP_BACK_IMAGE,
			g_param_spec_string ("back-image", NULL, NULL, "beige.png",
					     (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (
			gobject_class, PROP_HONORS_IMAGE,
			g_param_spec_string ("honors-image", NULL, NULL, "bonded.png",
					     (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (
			gobject_class, PROP_JOKER_IMAGE,
			g_param_spec_string ("joker-image", NULL, NULL, "gnome.png",
					     (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (
			gobject_class, PROP_RANKS_IMAGE,
			g_param_spec_string ("ranks-image", NULL, NULL, "bold-09x14.png",
					     (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (
			gobject_class, PROP_SUITS_SMALL_IMAGE,
			g_param_spec_string ("suits-small-image", NULL, NULL, "knuth-09x10.png",
					     (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (
			gobject_class, PROP_SUITS_MEDIUM_IMAGE,
			g_param_spec_string ("suits-medium-image", NULL, NULL, "knuth-18x21.png",
					     (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (
			gobject_class, PROP_SUITS_LARGE_IMAGE,
			g_param_spec_string ("suits-large-image", NULL, NULL, "knuth-21x25.png",
					     (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	gcardgame_image_deck_signals [PIXBUFS_CHANGED] =
                g_signal_new ("pixbufs_changed",
                              G_TYPE_FROM_CLASS (klass),
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GCardGameImageDeckClass, pixbufs_changed),
                              NULL,
			      NULL,
                              gcardgame_marshal_VOID__VOID,
                              G_TYPE_NONE,
			      1,
			      G_TYPE_INT);
}

static void
gcardgame_image_deck_instance_init (GCardGameImageDeck      *deck,
				    GCardGameImageDeckClass *klass)
{
	deck->priv = g_new0 (GCardGameImageDeckPrivate, 1);

	/*
	 * FIXME:
	 * 	Am I being dense here? Surely default_value in
	 * 	g_param_spec_string should make this not neccessary?
	 */
#if 1
	deck->priv->back_file         = g_strdup ("beige.png");
	deck->priv->honors_file       = g_strdup ("bonded.png");
	deck->priv->joker_file        = g_strdup ("gnome.png");
	deck->priv->ranks_file        = g_strdup ("bold-09x14.png");
	deck->priv->suits_small_file  = g_strdup ("knuth-09x10.png");
	deck->priv->suits_medium_file = g_strdup ("knuth-18x21.png");
	deck->priv->suits_large_file  = g_strdup ("knuth-21x25.png");
#endif

	deck->priv->layout_needs_calc = TRUE;
}

GType
gcardgame_image_deck_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (GCardGameImageDeckClass),
			NULL,
			NULL,
			(GClassInitFunc) gcardgame_image_deck_class_init,
			NULL,
			NULL,
			sizeof (GCardGameImageDeck),
			0,
			(GInstanceInitFunc) gcardgame_image_deck_instance_init,
			NULL
		};

		type = g_type_register_static (G_TYPE_OBJECT, "GCardGameImageDeck",
					       &info, 0);
	}

	return type;
}

GCardGameImageDeck *
gcardgame_image_deck_new (void)
{
	GCardGameImageDeck *deck;

	deck = g_object_new (GCARDGAME_TYPE_IMAGE_DECK, NULL);

	return deck;
}
