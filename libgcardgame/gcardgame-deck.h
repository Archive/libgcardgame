/*
 * gcardgame-deck.h: card game writing library.
 *
 * Copyright (C) 2002 Mark McLoughlin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *     Mark McLoughlin <mark@skynet.ie>
 */

#ifndef __GCARDGAME_DECK_H__
#define __GCARDGAME_DECK_H__

#include <glib/gmacros.h>
#include <glib/gslist.h>

G_BEGIN_DECLS

typedef enum {
	GCARDGAME_SUIT_NONE = 0,
	GCARDGAME_SUIT_CLUBS,
	GCARDGAME_SUIT_DIAMONDS,
	GCARDGAME_SUIT_HEARTS,
	GCARDGAME_SUIT_SPADES,
	GCARDGAME_SUIT_RED,     /* RED and BLACK are specfically for jokers */
	GCARDGAME_SUIT_BLACK
} GCardGameCardSuit;

typedef enum {
	GCARDGAME_VALUE_NONE = 0,
	GCARDGAME_VALUE_ACE,
	GCARDGAME_VALUE_TWO,
	GCARDGAME_VALUE_THREE,
	GCARDGAME_VALUE_FOUR,
	GCARDGAME_VALUE_FIVE,
	GCARDGAME_VALUE_SIX,
	GCARDGAME_VALUE_SEVEN,
	GCARDGAME_VALUE_EIGHT,
	GCARDGAME_VALUE_NINE,
	GCARDGAME_VALUE_TEN,
	GCARDGAME_VALUE_JACK,
	GCARDGAME_VALUE_QUEEN,
	GCARDGAME_VALUE_KING,
	GCARDGAME_VALUE_JOKER
} GCardGameCardValue;

typedef enum {
	GCARDGAME_DECK_FLAG_EMPTY		= 1 << 0,
	GCARDGAME_DECK_FLAG_NO_JOKERS		= 1 << 1,
} GCardGameDeckFlags;

typedef enum {
	GCARDGAME_CARD_FLAG_FACE_UP             = 1 << 0,
} GCardGameCardFlags;


typedef gulong GCardGameCard;
typedef GSList GCardGameStack; /* of GCardGameCards */


#define GCARDGAME_CARD(suit, value)		((suit) << 16 | (value))
#define GCARDGAME_CARD_SUIT(card)		(((card) & 0xff0000) >> 16)
#define GCARDGAME_CARD_VALUE(card)		((card) & 0xffff)

#define GCARDGAME_CARD_FACE_UP(card)		((card) & (GCARDGAME_CARD_FLAG_FACE_UP << 24))
#define GCARDGAME_CARD_FACE_DOWN(card)		(!((card) & (GCARDGAME_CARD_FLAG_FACE_UP << 24)))

#define GCARDGAME_CARD_SET_FACE_UP(card)	((card) | (GCARDGAME_CARD_FLAG_FACE_UP << 24))
#define GCARDGAME_CARD_SET_FACE_DOWN(card)	((card) & ~(GCARDGAME_CARD_FLAG_FACE_UP << 24))

#define GCARDGAME_EMPTY_CARD()			(GCARDGAME_CARD (GCARDGAME_SUIT_NONE, GCARDGAME_VALUE_NONE))
#define GCARDGAME_MAX_CARD()			(0x01ffffff)

typedef struct _GCardGameDeck {
	GCardGameCard  cards [54];

	guint          num;
} GCardGameDeck;


GCardGameDeck       *gcardgame_deck_new           (GCardGameDeckFlags  flags);

void                 gcardgame_deck_shuffle       (GCardGameDeck      *deck);

GCardGameCard        gcardgame_deck_bottom        (GCardGameDeck      *deck);
GCardGameCard        gcardgame_deck_top           (GCardGameDeck      *deck);
GCardGameCard        gcardgame_deck_random        (GCardGameDeck      *deck);

GCardGameStack    **gcardgame_deck_split          (GCardGameDeck      *deck,
						   int                 num_stacks);

void                gcardgame_stack_freev         (GCardGameStack    **stack_array);

GCardGameStack      *gcardgame_stack_append_deck  (GCardGameStack     *stack,
						   GCardGameDeck      *deck);
GCardGameStack      *gcardgame_stack_append_card  (GCardGameStack     *stack,
						   GCardGameCard       card);

GCardGameStack      *gcardgame_stack_prepend_deck (GCardGameStack     *stack,
						   GCardGameDeck      *deck);
GCardGameStack      *gcardgame_stack_prepend_card (GCardGameStack     *stack,
						   GCardGameCard       card);

char                *gcardgame_card_string        (GCardGameCard       card);

G_END_DECLS

#endif /* __GCARDGAME_DECK_H__ */
