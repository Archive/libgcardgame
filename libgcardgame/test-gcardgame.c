/*
 * test-gcardgame.c: card game writing library test program.
 *
 * Copyright (C) 2002 Mark McLoughlin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *     Mark McLoughlin <mark@skynet.ie>
 */

#include <config.h>
#include <stdio.h>

#include <glib.h>
#include <gtk/gtk.h>

#include "gcardgame-deck.h"
#include "gcardgame-image-deck.h"

static void
dump_deck (GCardGameDeck *deck,
	   int            index)
{
	int i;

	printf ("Deck Dump: %d\n", index);

	for (i = 0; i < 54; i++) {
		char *tmp;

		tmp = gcardgame_card_string (deck->cards [i]);

		printf ("%s", tmp);

		g_free (tmp);

		if (!i || (i + 1) % 3)
			printf ("\t\t");
		else
			printf ("\n");
	}

	printf ("\n\n");
}

static void
dump_stack (GCardGameStack *stack,
	    int             index)
{
	GSList *l;
	int     i = 0;

	printf ("Stack Dump: %d\n", index);

	for (l = stack; l; l = l->next, i++) {
		GCardGameCard  card;
		char          *tmp;

		card = GPOINTER_TO_UINT (l->data);

		tmp = gcardgame_card_string (card);

		printf ("%s", tmp);

		g_free (tmp);

		if (!i || (i + 1) % 3)
			printf ("\t\t");
		else
			printf ("\n");
	}

	printf ("\n\n");
}

static void
test_deck (GCardGameDeckFlags flags)
{
	GCardGameDeck   *deck;
	GCardGameCard    card;
	GCardGameStack **stack_array;
	int              deck_index = 1;
	int              stack_index = 1;
	int              i;

	deck = gcardgame_deck_new (flags);

	if (flags & GCARDGAME_DECK_FLAG_EMPTY)
		g_assert (deck->num == 0);

	else if (flags & GCARDGAME_DECK_FLAG_NO_JOKERS)
		g_assert (deck->num == 52);
	else 
		g_assert (deck->num == 54);

	dump_deck (deck, deck_index++);

	gcardgame_deck_shuffle (deck);

	g_assert (gcardgame_deck_top (deck) == deck->cards [0]);

	if (flags & GCARDGAME_DECK_FLAG_EMPTY)
		g_assert (gcardgame_deck_bottom (deck) == GCARDGAME_VALUE_NONE);
	else if (flags & GCARDGAME_DECK_FLAG_NO_JOKERS)
		g_assert (gcardgame_deck_bottom (deck) == deck->cards [51]);
	else
		g_assert (gcardgame_deck_bottom (deck) == deck->cards [53]);

	if (flags & GCARDGAME_DECK_FLAG_EMPTY)
		g_assert (gcardgame_deck_random (deck) == GCARDGAME_VALUE_NONE);
	else
		g_assert (gcardgame_deck_random (deck) != GCARDGAME_VALUE_NONE);

	dump_deck (deck, deck_index++);

	stack_array = gcardgame_deck_split (deck, 8);

	if (!(flags & GCARDGAME_DECK_FLAG_EMPTY))
		g_assert (stack_array);

	for (i = 0; stack_array && stack_array [i]; i++)
		dump_stack (stack_array [i], stack_index++);

	gcardgame_stack_freev (stack_array);

	card = gcardgame_deck_random (deck);

	g_assert (GCARDGAME_CARD_FACE_DOWN (card));
	g_assert (!GCARDGAME_CARD_FACE_UP (card));

	card = GCARDGAME_CARD_SET_FACE_UP (card);

	g_assert (GCARDGAME_CARD_FACE_UP (card));
	g_assert (!GCARDGAME_CARD_FACE_DOWN (card));

	card = GCARDGAME_CARD_SET_FACE_DOWN (card);

	g_assert (GCARDGAME_CARD_FACE_DOWN (card));
	g_assert (!GCARDGAME_CARD_FACE_UP (card));

	g_free (deck);
}

static gboolean
quit_cb (GtkWidget   *widget,
	 GdkEventAny *event,
	 gpointer     dummy)
{
	gtk_widget_destroy (widget);

	gtk_main_quit ();

	return TRUE;
}

static void
test_card_images (GnomeCanvasGroup *group)
{
	GCardGameImageDeck  *images;
	GCardGameDeck       *deck;
	GnomeCanvasItem     *item;
	GCardGameStack     **stack_array;
	GSList              *l;
	int                  i, j;

	images= gcardgame_image_deck_new ();

	g_assert (images);

	deck = gcardgame_deck_new (0);

	stack_array = gcardgame_deck_split (deck, 2);

	for (i = 0; stack_array && stack_array [i]; i++)
		for (l = stack_array [i], j = 0; l; l = l->next, j++)
			item = gnome_canvas_item_new (group,
						      GCARDGAME_TYPE_CARD_IMAGE,
						      "x", j * 20.0,
						      "y", (j % 2 ? 10.0 : 50.0) + i * 200,
						      "deck", images,
						      "card", GCARDGAME_CARD_SET_FACE_UP (GPOINTER_TO_UINT (l->data)),
						      NULL);

	g_free (deck);

	g_object_unref (G_OBJECT (images));
}

static void
test_canvas_deck (void)
{
	GnomeCanvasGroup *root;
	GtkWidget        *app;
	GtkWidget        *canvas;
	GtkWidget        *frame;

	app = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_policy (GTK_WINDOW (app), TRUE, TRUE, FALSE);

	gtk_signal_connect (GTK_OBJECT (app), "delete_event",
			    (GtkSignalFunc) quit_cb, NULL);



	gtk_widget_push_colormap (gdk_rgb_get_cmap ());

	canvas = gnome_canvas_new_aa ();

	root = gnome_canvas_root (GNOME_CANVAS (canvas));

	gtk_widget_pop_colormap ();

	test_card_images (root);

	frame = gtk_frame_new (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT);
	gtk_widget_show (frame);

	gtk_container_add (GTK_CONTAINER (frame), canvas);

	gtk_widget_set_usize (canvas, 600, 400);
	gnome_canvas_set_scroll_region (GNOME_CANVAS (canvas), 0, 0, 600, 400);
	gtk_widget_show (canvas);

	gtk_container_add (GTK_CONTAINER (app), frame);

	gtk_widget_show (app);
}

int
main (int argc, char **argv)
{
	g_type_init_with_debug_flags (G_TYPE_DEBUG_OBJECTS);

	gtk_init (&argc, &argv);

	printf ("==== No Flags ====\n");

	test_deck (0);

	printf ("==== Empty Deck ====\n");

	test_deck (GCARDGAME_DECK_FLAG_EMPTY);

	printf ("==== No Jokers Deck ====\n");

	test_deck (GCARDGAME_DECK_FLAG_NO_JOKERS);

	printf ("==== Empty Deck with No Jokers ====\n");

	test_deck (GCARDGAME_DECK_FLAG_EMPTY | GCARDGAME_DECK_FLAG_NO_JOKERS);

	test_canvas_deck ();

	gtk_main ();

	return 0;
}
