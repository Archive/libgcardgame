/*
 * gcardgame-deck.c: card game writing library.
 *
 * Copyright (C) 2002 Mark McLoughlin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *     Mark McLoughlin <mark@skynet.ie>
 */

#include <config.h>
#include <stdio.h>
#include <string.h>
#include <libintl.h>

#include <glib.h>

#include "gcardgame-deck.h"
#include "gcardgame-i18n.h"

GCardGameDeck *
gcardgame_deck_new (GCardGameDeckFlags flags)
{
	GCardGameDeck      *retval;
	GCardGameCardSuit   suit;
	GCardGameCardValue  value;

	retval = g_new0 (GCardGameDeck, 1);

	if (flags & GCARDGAME_DECK_FLAG_EMPTY)
		return retval;

	for (suit = GCARDGAME_SUIT_CLUBS; suit <= GCARDGAME_SUIT_SPADES; suit++)
		for (value = GCARDGAME_VALUE_ACE; value <= GCARDGAME_VALUE_KING; value++)
			retval->cards [retval->num++] = GCARDGAME_CARD (suit, value);

	if (flags & GCARDGAME_DECK_FLAG_NO_JOKERS)
		return retval;

	retval->cards [retval->num++] = GCARDGAME_CARD (GCARDGAME_SUIT_RED,
							GCARDGAME_VALUE_JOKER);
	retval->cards [retval->num++] = GCARDGAME_CARD (GCARDGAME_SUIT_BLACK,
							GCARDGAME_VALUE_JOKER);

	g_assert (retval->num == 54);

	return retval;
}

void
gcardgame_deck_shuffle (GCardGameDeck *deck)
{
	GCardGameStack *stack;
	int             length;
	int             i;

	stack = gcardgame_stack_append_deck (NULL, deck);

	memset (deck->cards, 0, sizeof (GCardGameCard) * 54);

	length = g_slist_length (stack);

	for (i = 0; i < 54 && length > 0; i++, length--) {
		GSList *link;
		int     index;

		index = g_random_int_range (0, length);

		link = g_slist_nth (stack, index);

		deck->cards [i] = GPOINTER_TO_UINT (link->data);

		stack = g_slist_delete_link (stack, link);
	}
}

GCardGameCard
gcardgame_deck_bottom (GCardGameDeck *deck)
{
	int i;

	for (i = 53; i > 0 && !deck->cards [i]; i--); /* skip empy slots */

	return deck->cards [i];
}

GCardGameCard
gcardgame_deck_top (GCardGameDeck *deck)
{
	return deck->cards [0];
}

GCardGameCard
gcardgame_deck_random (GCardGameDeck *deck)
{
	int i;

	for (i = 53; i > 0 && !deck->cards [i]; i--); /* skip empy slots */

	if (i == 0)
		return GCARDGAME_VALUE_NONE;

	return deck->cards [g_random_int_range (0, i)];
}

GCardGameStack **
gcardgame_deck_split (GCardGameDeck *deck,
		      int            num_stacks)
{
	GCardGameStack **stack_array;
	int              num_cards;
	int              extra_cards;
	int              i, j = 0;

	if (!deck || !deck->num)
		return NULL;

	g_return_val_if_fail (num_stacks && num_stacks < deck->num, NULL);

	if (num_stacks == -1) 
		num_stacks = g_random_int_range (1, deck->num);

	stack_array = g_new0 (GCardGameStack *, num_stacks + 1);

	num_cards   = deck->num / num_stacks;
	extra_cards = deck->num % num_stacks;

	for (i = 0; i < num_stacks; i++) {
		int end;
		
		end = j + num_cards;

		if (extra_cards-- > 0)
			end++;

		for (;j < end; j++)
			stack_array [i] =
				gcardgame_stack_append_card (stack_array [i],
							     deck->cards [j]);

	}

	g_assert (j == deck->num);

	return stack_array;
}

void
gcardgame_stack_freev (GCardGameStack **stack_array)
{
	int i;

	if (!stack_array)
		return;

	for (i = 0; stack_array [i]; i++)
		g_slist_free (stack_array [i]);

	g_free (stack_array);
}

GCardGameStack *
gcardgame_stack_append_deck (GCardGameStack *stack,
			     GCardGameDeck  *deck)
{
	GSList *tmp = NULL;
	GSList *retval;
	int     i;

	for (i = 53; i >= 0 && !deck->cards [i]; i--); /* skip empy slots */

	for (; i >= 0; i--)
		tmp = g_slist_prepend (tmp, GUINT_TO_POINTER (deck->cards [i]));

	retval = g_slist_concat (stack, tmp);

	return retval;
}

GCardGameStack *
gcardgame_stack_append_card (GCardGameStack *stack,
			     GCardGameCard   card)
{
	return g_slist_append (stack, GUINT_TO_POINTER (card));
}

GCardGameStack *
gcardgame_stack_prepend_deck (GCardGameStack *stack,
			      GCardGameDeck  *deck)
{
	GSList *tmp = NULL;
	GSList *retval;
	int     i;

	for (i = 0; i < 54 && deck->cards [i]; i++)
		tmp = g_slist_prepend (tmp, GUINT_TO_POINTER (deck->cards [i]));

	retval = g_slist_concat (stack, tmp);

	g_slist_free (tmp);

	return retval;
}

GCardGameStack *
gcardgame_stack_prepend_card (GCardGameStack *stack,
			      GCardGameCard   card)
{
	return g_slist_prepend (stack, GUINT_TO_POINTER (card));
}
	
gchar *
gcardgame_card_string (GCardGameCard card)
{
	char *retval = NULL;
	char *value = NULL;
	char *suit = NULL;

	switch (GCARDGAME_CARD_VALUE (card)) {
	case GCARDGAME_VALUE_ACE:
		value = _("Ace");
		break;
	case GCARDGAME_VALUE_TWO:
		value = _("Deuce");
		break;
	case GCARDGAME_VALUE_THREE:
		value = _("Three");
		break;
	case GCARDGAME_VALUE_FOUR:
		value = _("Four");
		break;
	case GCARDGAME_VALUE_FIVE:
		value = _("Five");
		break;
	case GCARDGAME_VALUE_SIX:
		value = _("Six");
		break;
	case GCARDGAME_VALUE_SEVEN:
		value = _("Seven");
		break;
	case GCARDGAME_VALUE_EIGHT:
		value = _("Eight");
		break;
	case GCARDGAME_VALUE_NINE:
		value = _("Nine");
		break;
	case GCARDGAME_VALUE_TEN:
		value = _("Ten");
		break;
	case GCARDGAME_VALUE_JACK:
		value = _("Jack");
		break;
	case GCARDGAME_VALUE_QUEEN:
		value = _("Queen");
		break;
	case GCARDGAME_VALUE_KING:
		value = _("King");
		break;
	case GCARDGAME_VALUE_JOKER:
		retval = g_strdup_printf (_("The %s Joker"),
				GCARDGAME_CARD_SUIT (card) == GCARDGAME_SUIT_RED ?
					_("Red") : _("Black"));
		break;
	case GCARDGAME_VALUE_NONE:
		retval = g_strdup_printf (_("An Empty Card"));
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	if (retval)
		return retval;

	switch (GCARDGAME_CARD_SUIT (card)) {
	case GCARDGAME_SUIT_CLUBS:
		suit = _("Clubs");
		break;
	case GCARDGAME_SUIT_DIAMONDS:
		suit = _("Diamonds");
		break;
	case GCARDGAME_SUIT_HEARTS:
		suit = _("Hearts");
		break;
	case GCARDGAME_SUIT_SPADES:
		suit = _("Spades");
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	retval = g_strdup_printf (_("The %s of %s"), value, suit);

	return retval;
}
