/*
 * gcardgame-card-image.h: card game writing library.
 *
 * Copyright (C) 2002 Mark McLoughlin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *     Mark McLoughlin <mark@skynet.ie>
 */

#ifndef __GCARDGAME_CARD_IMAGE_H__
#define __GCARDGAME_CARD_IMAGE_H__

#include <glib/gmacros.h>
#include <libgnomecanvas/gnome-canvas.h>

#include "gcardgame-deck.h"

G_BEGIN_DECLS

#define GCARDGAME_TYPE_CARD_IMAGE         (gcardgame_card_image_get_type ())
#define GCARDGAME_CARD_IMAGE(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), GCARDGAME_TYPE_CARD_IMAGE, GCardGameCardImage))
#define GCARDGAME_CARD_IMAGE_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), GCARDGAME_TYPE_CARD_IMAGE, GCardGameCardImageClass))
#define GCARDGAME_IS_CARD_IMAGE(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), GCARDGAME_TYPE_CARD_IMAGE))
#define GCARDGAME_IS_CARD_IMAGE_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), GCARDGAME_TYPE_CARD_IMAGE))
#define GCARDGAME_CARD_IMAGE_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GCARDGAME_TYPE_CARD_IMAGE, GCardGameCardImageClass))

typedef struct _GCardGameCardImage          GCardGameCardImage;
typedef struct _GCardGameCardImageClass     GCardGameCardImageClass;
typedef struct _GCardGameCardImagePrivate   GCardGameCardImagePrivate;

struct _GCardGameCardImage {
	GnomeCanvasGroup            parent_instance;

	GCardGameCardImagePrivate  *priv;
};

struct _GCardGameCardImageClass {
	GnomeCanvasGroupClass       parent_class;
};

GType                gcardgame_card_image_get_type   (void) G_GNUC_CONST;

G_END_DECLS

#endif /* __GCARDGAME_CARD_IMAGE_H__ */
