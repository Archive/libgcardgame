/*
 * gcardgame-card-image.c: card game writing library.
 *
 * Copyright (C) 2002 Mark McLoughlin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors:
 *     Mark McLoughlin <mark@skynet.ie>
 */

/*
 * This is broadly a port to the canvas of the gdk-card-image library by:
 *
 * 	Felix Bellaby <felix@pooh.u-net.com> and
 *	Ryu Changwoo <cwryu@eve.kaist.ac.kr>
 *
 * see gnome-games/freecell/gdk-card-image
 */

#include "config.h"

#include <libgnomecanvas/gnome-canvas.h>
#include <libgnomecanvas/gnome-canvas-bpath.h>
#include <libgnomecanvas/gnome-canvas-path-def.h>
#include <libgnomecanvas/gnome-canvas-pixbuf.h>
#include <libgnomecanvas/gnome-canvas-util.h>

#include "gcardgame-card-image.h"
#include "gcardgame-image-deck.h"

#undef GCARDGAME_CARD_IMAGE_DEBUG

struct _GCardGameCardImagePrivate {
	GCardGameImageDeck *deck;
	GCardGameCard       card;

	GnomeCanvasItem    *surround_item;
	GnomeCanvasItem    *rank_item_nw;
	GnomeCanvasItem    *rank_item_se;
	GnomeCanvasItem    *small_suit_item_nw;
	GnomeCanvasItem    *small_suit_item_se;
	GSList             *centre_items;
};

static GObjectClass *parent_class;

enum {
	PROP_0,
	PROP_DECK,
	PROP_CARD
};

static void
draw_back (GCardGameCardImage   *image,
	   GCardGameImageLayout *layout)
{
}

static void
draw_surround (GCardGameCardImage   *image,
	       GCardGameImageLayout *layout)
{
	GnomeCanvasPathDef *path_def;

	path_def = gnome_canvas_path_def_new ();

	gnome_canvas_path_def_moveto (path_def,
				      layout->corner, 0.0);

	gnome_canvas_path_def_lineto (path_def,
				      layout->width - layout->corner, 0.0);

	gnome_canvas_path_def_curveto (path_def,
				       layout->width, 0.0,
				       layout->width, 0.0,
				       layout->width, layout->corner);

	gnome_canvas_path_def_lineto (path_def,
				      layout->width, layout->height - layout->corner);

	gnome_canvas_path_def_curveto (path_def,
				       layout->width, layout->height,
				       layout->width, layout->height,
				       layout->width - layout->corner, layout->height);

	gnome_canvas_path_def_lineto (path_def,
				      layout->corner, layout->height);

	gnome_canvas_path_def_curveto (path_def,
				       0.0, layout->height,
				       0.0, layout->height,
				       0.0, layout->height - layout->corner);

	gnome_canvas_path_def_lineto (path_def,
				      0.0, layout->corner);

	gnome_canvas_path_def_curveto (path_def,
				       0.0, 0.0,
				       0.0, 0.0,
				       layout->corner, 0.0);

	gnome_canvas_path_def_closepath (path_def);

	image->priv->surround_item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (image),
							    GNOME_TYPE_CANVAS_BPATH,
							    "bpath", path_def,
							    "fill_color", "white",
							    "outline_color", "black",
							    NULL);

	gnome_canvas_path_def_unref (path_def);
}

static void
draw_ranks (GCardGameCardImage   *image,
	    GCardGameImageLayout *layout)
{
	GdkPixbuf *pixbuf, *rot_pixbuf;
	gdouble    cx, cy;
	int        suit, rank;

	suit = GCARDGAME_CARD_SUIT (image->priv->card);
	rank = GCARDGAME_CARD_VALUE (image->priv->card);

	pixbuf     = gcardgame_image_deck_rank_pixbuf (image->priv->deck, suit, rank, FALSE);
	rot_pixbuf = gcardgame_image_deck_rank_pixbuf (image->priv->deck, suit, rank, TRUE);

	cx = layout->width  - layout->rx - gdk_pixbuf_get_width (pixbuf);
	cy = layout->height - layout->ry - gdk_pixbuf_get_height (pixbuf);

	image->priv->rank_item_nw = gnome_canvas_item_new (GNOME_CANVAS_GROUP (image),
							   GNOME_TYPE_CANVAS_PIXBUF,
							   "pixbuf", pixbuf,
							   "x", layout->rx,
							   "y", layout->ry,
							   NULL);

	image->priv->rank_item_se = gnome_canvas_item_new (GNOME_CANVAS_GROUP (image),
							   GNOME_TYPE_CANVAS_PIXBUF,
							   "pixbuf", rot_pixbuf,
							   "x", cx,
							   "y", cy,
							   NULL);

	g_object_unref (G_OBJECT (pixbuf));
	g_object_unref (G_OBJECT (rot_pixbuf));
}

static void
draw_small_suits (GCardGameCardImage   *image,
		  GCardGameImageLayout *layout)
{
	GdkPixbuf *pixbuf, *rot_pixbuf;
	gdouble    cx, cy;
	int        suit, rank;

	suit = GCARDGAME_CARD_SUIT (image->priv->card);
	rank = GCARDGAME_CARD_VALUE (image->priv->card);

	if (rank == GCARDGAME_VALUE_JOKER)
		return;

	pixbuf     = gcardgame_image_deck_small_suit_pixbuf (image->priv->deck, suit, FALSE);
	rot_pixbuf = gcardgame_image_deck_small_suit_pixbuf (image->priv->deck, suit, TRUE);

	cx = layout->width  - layout->sx - gdk_pixbuf_get_width (pixbuf);
	cy = layout->height - layout->sy - gdk_pixbuf_get_height (pixbuf);

	image->priv->small_suit_item_nw = gnome_canvas_item_new (GNOME_CANVAS_GROUP (image),
								 GNOME_TYPE_CANVAS_PIXBUF,
								 "pixbuf", pixbuf,
								 "x", layout->sx,
								 "y", layout->sy,
								 NULL);

	image->priv->small_suit_item_se = gnome_canvas_item_new (GNOME_CANVAS_GROUP (image),
								 GNOME_TYPE_CANVAS_PIXBUF,
								 "pixbuf", rot_pixbuf,
								 "x", cx,
								 "y", cy,
								 NULL);

	g_object_unref (G_OBJECT (pixbuf));
	g_object_unref (G_OBJECT (rot_pixbuf));
}

static void
draw_centre_items (GCardGameCardImage   *image,
		   GCardGameImageLayout *layout)
{
	GnomeCanvasPoints *points;
	GdkPixbuf         *pixbuf = NULL, *rot_pixbuf = NULL;
	int                suit, rank;
	int                i, index = 0;

	suit = GCARDGAME_CARD_SUIT (image->priv->card);
	rank = GCARDGAME_CARD_VALUE (image->priv->card);

	switch (rank) {
	case GCARDGAME_VALUE_ACE:
		pixbuf = gcardgame_image_deck_large_suit_pixbuf (image->priv->deck, suit, FALSE);
		break;
	case GCARDGAME_VALUE_JOKER:
		pixbuf = gcardgame_image_deck_joker_pixbuf (image->priv->deck, FALSE);
		break;
	case GCARDGAME_VALUE_TWO:
	case GCARDGAME_VALUE_THREE:
	case GCARDGAME_VALUE_FOUR:
	case GCARDGAME_VALUE_FIVE:
	case GCARDGAME_VALUE_SIX:
	case GCARDGAME_VALUE_SEVEN:
	case GCARDGAME_VALUE_EIGHT:
	case GCARDGAME_VALUE_NINE:
	case GCARDGAME_VALUE_TEN:
		pixbuf     = gcardgame_image_deck_medium_suit_pixbuf (image->priv->deck, suit, FALSE);
		rot_pixbuf = gcardgame_image_deck_medium_suit_pixbuf (image->priv->deck, suit, TRUE);
		break;
	case GCARDGAME_VALUE_JACK:
	case GCARDGAME_VALUE_QUEEN:
	case GCARDGAME_VALUE_KING:
		pixbuf     = gcardgame_image_deck_honor_pixbuf (image->priv->deck, suit, rank, FALSE);
		rot_pixbuf = gcardgame_image_deck_honor_pixbuf (image->priv->deck, suit, rank, TRUE);
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	points = gnome_canvas_points_new (10);

	switch (rank) {
	case GCARDGAME_VALUE_ACE:
	case GCARDGAME_VALUE_JOKER:
		points->coords [index++] = (layout->width - gdk_pixbuf_get_width (pixbuf)) / 2;
		points->coords [index++] = (layout->height - gdk_pixbuf_get_height (pixbuf)) / 2;
		break;
	case GCARDGAME_VALUE_THREE:
		points->coords [index++] = layout->x1;
		points->coords [index++] = layout->y4;
	case GCARDGAME_VALUE_TWO: /* drop through */
		points->coords [index++] = layout->x1;
		points->coords [index++] = layout->y0;

		points->coords [index++] = - layout->x1;
		points->coords [index++] = - layout->y8;
		break;
	case GCARDGAME_VALUE_FIVE:
		points->coords [index++] = layout->x1;
		points->coords [index++] = layout->y4;
	case GCARDGAME_VALUE_FOUR: /* drop through */
		points->coords [index++] = layout->x0; 
		points->coords [index++] = layout->y0;

		points->coords [index++] = layout->x2;
		points->coords [index++] = layout->y0;

		points->coords [index++] = - layout->x0;
		points->coords [index++] = - layout->y8;

		points->coords [index++] = - layout->x2;
		points->coords [index++] = - layout->y8;
		break;
	case GCARDGAME_VALUE_EIGHT:
		points->coords [index++] = - layout->x1;
		points->coords [index++] = - layout->y6 ;
	case GCARDGAME_VALUE_SEVEN: /* drop through */
		points->coords [index++] = layout->x1;
		points->coords [index++] = layout->y2;
	case GCARDGAME_VALUE_SIX: /* drop through */
		points->coords [index++] = layout->x0; 
		points->coords [index++] = layout->y0;

		points->coords [index++] = layout->x2;
		points->coords [index++] = layout->y0;

		points->coords [index++] = layout->x0;
		points->coords [index++] = layout->y4;

		points->coords [index++] = layout->x2;
		points->coords [index++] = layout->y4;

		points->coords [index++] = - layout->x0;
		points->coords [index++] = - layout->y8;

		points->coords [index++] = - layout->x2;
		points->coords [index++] = - layout->y8;
		break;
	case GCARDGAME_VALUE_NINE:
	case GCARDGAME_VALUE_TEN: /* drop through */
		points->coords [index++] = layout->x0; 
		points->coords [index++] = layout->y0;

		points->coords [index++] = layout->x2;
		points->coords [index++] = layout->y0;

		points->coords [index++] = layout->x0;
		points->coords [index++] = layout->y3;

		points->coords [index++] = layout->x2;
		points->coords [index++] = layout->y3;

		points->coords [index++] = - layout->x0;
		points->coords [index++] = - layout->y5;

		points->coords [index++] = - layout->x2;
		points->coords [index++] = - layout->y5;

		points->coords [index++] = - layout->x0;
		points->coords [index++] = - layout->y8;

		points->coords [index++] = - layout->x2;
		points->coords [index++] = - layout->y8;

		if (rank == GCARDGAME_VALUE_NINE) {
			points->coords [index++] = layout->x1;
			points->coords [index++] = layout->y4;
		} else {
			points->coords [index++] = layout->x1;
			points->coords [index++] = layout->y1;

			points->coords [index++] = - layout->x1;
			points->coords [index++] = - layout->y7;
		}
		break;
	case GCARDGAME_VALUE_JACK:
	case GCARDGAME_VALUE_QUEEN: /* drop through */
	case GCARDGAME_VALUE_KING:
		points->coords [index++] = (layout->width - gdk_pixbuf_get_width (pixbuf)) / 2;
		points->coords [index++] = (layout->height + 1) / 2 - gdk_pixbuf_get_height (pixbuf);
		points->coords [index++] = - (layout->width - gdk_pixbuf_get_width (pixbuf)) / 2;
		points->coords [index++] = - ((layout->height + 1) / 2 - 1);
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	g_assert (index % 2 == 0);

	for (i = 0; i < index; i += 2) {
		GnomeCanvasItem *item;
		GdkPixbuf       *pbuf;

		pbuf = points->coords [i] > 0 ? pixbuf : rot_pixbuf;

		item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (image),
					      GNOME_TYPE_CANVAS_PIXBUF,
			 		      "pixbuf", pbuf,
					      "x", ABS (points->coords [i]),
					      "y", ABS (points->coords [i + 1]),
					      NULL);

		image->priv->centre_items = g_slist_prepend (image->priv->centre_items, item);
	}

	gnome_canvas_points_free (points);
}

static void
draw_card (GCardGameCardImage *image)
{
	GCardGameImageLayout *layout;

	layout = gcardgame_image_deck_get_layout (image->priv->deck);

	draw_surround (image, layout);

	if (GCARDGAME_CARD_FACE_DOWN (image->priv->card)) {
		draw_back (image, layout);
		return;
	}

	if (GCARDGAME_CARD_VALUE (image->priv->card) == GCARDGAME_VALUE_NONE)
		return;

	draw_ranks (image, layout);
	draw_small_suits (image, layout);
	draw_centre_items (image, layout);
}

static void
gcardgame_card_image_update (GnomeCanvasItem *item,
			     double          *affine,
			     ArtSVP          *clip_path,
			     int              flags)
{
	if (flags & GNOME_CANVAS_UPDATE_REQUESTED)
		draw_card (GCARDGAME_CARD_IMAGE (item));


	GNOME_CANVAS_ITEM_CLASS (parent_class)->update (item, affine, clip_path, flags);
}

static void
gcardgame_card_image_set_property (GObject      *object,
				   guint         prop_id,
				   const GValue *value,
				   GParamSpec   *pspec)
{
	GCardGameCardImage *image = GCARDGAME_CARD_IMAGE (object);

	switch (prop_id) {
	case PROP_DECK:
		if (image->priv->deck)
			g_object_unref (image->priv->deck);

		image->priv->deck = g_object_ref (G_OBJECT (g_value_get_object (value)));

		gnome_canvas_item_request_update (GNOME_CANVAS_ITEM (object));
		break;
	case PROP_CARD:
		image->priv->card = g_value_get_ulong (value);

		gnome_canvas_item_request_update (GNOME_CANVAS_ITEM (object));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
gcardgame_card_image_get_property (GObject    *object,
				   guint       prop_id,
				   GValue     *value,
				   GParamSpec *pspec)
{
	GCardGameCardImage *image = GCARDGAME_CARD_IMAGE (object);

	switch (prop_id) {
	case PROP_DECK:
		g_value_set_object (value, image->priv->deck);
		break;
	case PROP_CARD:
		g_value_set_ulong (value, image->priv->card);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
gcardgame_card_image_finalize (GObject *object)
{
	GCardGameCardImage *image = GCARDGAME_CARD_IMAGE (object);

	/*
	 * A group's children get destroyed when it is destroyed.
	 */
	image->priv->surround_item      = NULL;
	image->priv->small_suit_item_nw = NULL;
	image->priv->small_suit_item_se = NULL;
	image->priv->rank_item_nw       = NULL;
	image->priv->rank_item_se       = NULL;

	if (image->priv->centre_items)
		g_slist_free (image->priv->centre_items);
	image->priv->centre_items = NULL;

	if (image->priv->deck)
		g_object_unref (G_OBJECT (image->priv->deck));
	image->priv->deck = NULL;

	g_free (image->priv);
	image->priv = NULL;

	parent_class->finalize (object);
}

static void
gcardgame_card_image_class_init (GCardGameCardImageClass *klass,
				 gpointer                 dummy)
{
	GObjectClass  *gobject_class = (GObjectClass *) klass;
	GnomeCanvasItemClass *item_class = (GnomeCanvasItemClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	gobject_class->finalize     = gcardgame_card_image_finalize;
	gobject_class->set_property = gcardgame_card_image_set_property;
	gobject_class->get_property = gcardgame_card_image_get_property;

	g_object_class_install_property (
			gobject_class, PROP_DECK,
			g_param_spec_object ("deck", NULL, NULL, GCARDGAME_TYPE_IMAGE_DECK,
					     (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (
			gobject_class, PROP_CARD,
			g_param_spec_ulong ("card", NULL, NULL,
					    GCARDGAME_EMPTY_CARD (),
					    GCARDGAME_MAX_CARD (),
					    GCARDGAME_EMPTY_CARD (),
					    (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	item_class->update = gcardgame_card_image_update;
}

static void
gcardgame_card_image_instance_init (GCardGameCardImage      *image,
				    GCardGameCardImageClass *klass)
{
	image->priv = g_new0 (GCardGameCardImagePrivate, 1);
}

GType
gcardgame_card_image_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (GCardGameCardImageClass),
			NULL,
			NULL,
			(GClassInitFunc) gcardgame_card_image_class_init,
			NULL,
			NULL,
			sizeof (GCardGameCardImage),
			0,
			(GInstanceInitFunc) gcardgame_card_image_instance_init,
			NULL
		};

		type = g_type_register_static (GNOME_TYPE_CANVAS_GROUP, "GCardGameCardImage",
					       &info, 0);
	}

	return type;
}
